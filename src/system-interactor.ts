import vscode from "vscode"

export const defaultSystemInteractor = {
    createTerminal: vscode.window.createTerminal,
    onDidCloseTerminal: vscode.window.onDidCloseTerminal,
}

export type SystemInteractorType = typeof defaultSystemInteractor;

export default {
    defaultSystemInteractor,
}
