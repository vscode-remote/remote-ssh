

export class HostInfo {

    hostName: string;

    user: string;

    constructor(hostName: string, user: string = undefined) {
        this.hostName = hostName
        this.user = user
    }

    toString() {
        if (this.user) {
            return `${this.user}@${this.hostName}`
        } else {
            return this.hostName
        }
    }

    /**
     * @returns hostinfoStr
     */
    static stringify(info: HostInfo) {
        if (info.user || info.hostName.toLowerCase() !== info.hostName) {

            let infoObj
            if (info.user) {
                infoObj = {
                    hostName: info.hostName,
                    user: info.user
                }
            } else {
                infoObj =  {
                    hostName: info.hostName
                }
            }
            
            const n = JSON.stringify(infoObj)
            return Buffer.from(n, "utf8").toString("hex");
        }

        return info.hostName;
    }

    /**
     * @param data hostinfoStr
     */
    static parse(data: string) {
        try {
            const strFromHex = Buffer.from(data, "hex").toString("utf8")
            const info = JSON.parse(strFromHex)
            if (info) {
                return new HostInfo(info.hostName, info.user)
            }
        } catch (e) { }

        return HostInfo.fromString(data);
    }

    static fromString(str: string) {
        const lastAtIndex = str.lastIndexOf("@")
        if (lastAtIndex > -1) {
            return new HostInfo(str.substring(lastAtIndex + 1), str.substring(0, lastAtIndex))
        } else {
            return new HostInfo(str)
        }
    }
}

export default {
    HostInfo,
}
