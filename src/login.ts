
import Logger from "./logger";
import { HostInfo } from "./host-info";

type BooleanLikeString = "yes" | "no" | string

export type confirmationFunction = (hostName: string, fingerprintData: string) => BooleanLikeString | PromiseLike<BooleanLikeString>

export type GetPassphraseFunction = () => string | PromiseLike<string>

export interface LoginResult {
    postAction?: string;
    response?: string;
    isPassword?: boolean;
    canceled?: boolean;
}

export const handleFingerprint = async (data: string, hostinfo: HostInfo, confirmationFunction: confirmationFunction, logger: Logger) => {
    const fingerprintIsRegex = /fingerprint\sis\s(.+)\./
    const result: LoginResult = { postAction: "keep" }
    const lines = data.trim().split("\n")

    if (
        lines.length >= 3 &&
        lines[lines.length - 1].indexOf("Are you sure you want to continue connecting") >= 0
    ) {
        let lastIndex = lines.length - 1

        while (--lastIndex >= 0 && !lines[lines.length - 1].search(fingerprintIsRegex)) {

        }

        if (lastIndex >= 0) {
            result.postAction = "consume"

            const fingerprintMatch = fingerprintIsRegex.exec(lines[lastIndex])
            const fingerprintData = fingerprintMatch ? fingerprintMatch[1] : data
            const isConfirmed = await confirmationFunction(hostinfo.hostName, fingerprintData)

            if (isConfirmed) {
                result.response = isConfirmed
            } else {
                result.canceled = true
            }
        }
    } else if (
        lines.length == 1 &&
        !(
            0 !== lines[0].indexOf("Warning: Permanently added") &&
            0 !== lines[0].indexOf("yes") &&
            0 !== lines[0].indexOf("no")
        )
    ) {
        result.postAction = "consume"
    }


    return result;
}

export const handlePassphrase = async (data: string, getPassphraseFunction: GetPassphraseFunction, logger: Logger) => {
    const result: LoginResult = { postAction: "keep" }
    const lines = data.trim().split("\n")

    if (lines.some(line => line.indexOf("Enter passphrase for") >= 0)) {
        result.postAction = "consume"

        const passphrase = await getPassphraseFunction()

        if (passphrase) {
            result.response = passphrase
            result.isPassword = true
        } else {
            result.canceled = true
        }
    } else if (lines.some(line => line.indexOf("Identity added:") >= 0)) {
        result.postAction = "consume"
    }


    return result
}

export const getExitCode = (data: string, uuid: string) => {
    const match = new RegExp(`${uuid}##([0-9]*)##`).exec(data)

    if (match) {
        try {
            const exitCode = parseInt(match[1])
            if (Number.isNaN(exitCode)) {
                return undefined
            } else {
                return exitCode
            }
        } catch (err) {
            return
        }
    }

    return 0
}

export const handlePassword = async (data: string, getPasswordFunction: GetPassphraseFunction, logger: Logger) => {
    const result: LoginResult = { postAction: "keep" }
    const lines = data.trim().split("\n")

    if (lines.some(line => line.indexOf("Password:") >= 0)) {
        result.postAction = "consume"

        const password = await getPasswordFunction()

        if (password) {
            result.response = password
            result.isPassword = true
        } else {
            result.canceled = true
        }
    }

    return result;
}

export const handle2FA = async (data: string, get2FACodeFunction: GetPassphraseFunction, logger: Logger) => {
    const result: LoginResult = { postAction: "keep" }
    const lines = data.trim().split("\n")

    if (lines.some(e => e.indexOf("Verification code:") >= 0)) {
        result.postAction = "consume"

        const code = await get2FACodeFunction()

        if (code) {
            result.response = code
            result.isPassword = true
        } else {
            result.canceled = true
        }
    }

    return result
}

export default {
    handleFingerprint,
    handlePassphrase,
    getExitCode,
    handlePassword,
    handle2FA,
}
