
import net from "net"

const endConnection = (socket: net.Socket) => {
    try {
        socket.removeAllListeners("connect")
        socket.removeAllListeners("error")
        socket.end()
        socket.destroy()
        socket.unref()
    } catch (err) {
        console.error(err)
    }
}

export const randomPort = () => {
    return 1025 + Math.floor(64510 * Math.random())
}

export const findFreePort = function (initialPort: number, maxTry: number, timeout: number): Promise<number> {
    let faild = false

    return new Promise((resolve) => {
        const timer = setTimeout(() => {
            if (!faild) {
                faild = true
                return faild
            }
            resolve(0)
        }, timeout)

        const cb = (port: number) => {
            if (!faild) {
                faild = true
                return faild
            }
            clearTimeout(timer)
            resolve(port)
        }

        const find = (_port: number, _maxTry: number) => {
            if (0 === _maxTry) {
                return cb(0)
            }

            const socket = new net.Socket()

            socket.once("connect", () => {
                endConnection(socket)
                find(_port + 1, _maxTry - 1)
            })

            socket.once("data", () => { })

            socket.once("error", (err) => {
                endConnection(socket)

                // @ts-ignore
                if ("ECONNREFUSED" !== err.code) {
                    find(_port + 1, _maxTry - 1)
                } else {
                    cb(_port)
                }
            })

            socket.connect(_port, "127.0.0.1")
        }

        find(initialPort, maxTry)

    })
}

export const isFreePort = async (port: number) => {
    return new Promise((resolve) => {
        const socket = new net.Socket()

        socket.once("connect", () => {
            endConnection(socket)
            resolve(false)
        })

        socket.once("data", () => { })

        socket.once("error", (err) => {
            endConnection(socket)
            // @ts-ignore
            resolve("ECONNREFUSED" === err.code)
        })

        socket.connect(port, "127.0.0.1")
    })
}

export default {
    randomPort,
    findFreePort,
    isFreePort,
}
