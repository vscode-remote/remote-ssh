
const createlockFileScript = (commitID: string) => {
	return `

	## Copyright (C) 2009 Przemyslaw Pawelczyk <przemoc@gmail.com>
	## This script is licensed under the terms of the MIT license.
	## https://opensource.org/licenses/MIT

	LOCKFILE="$VSCH_DIR/vscode-remote-lock.${commitID}"
	LOCKFD=99

	# PRIVATE
	_lock()             { flock -$1 $LOCKFD; }
	_no_more_locking()  { _lock u; _lock xn && rm -f $LOCKFILE; }
	_prepare_locking()  { eval "exec $LOCKFD>\\"$LOCKFILE\\""; trap _no_more_locking EXIT; }

	# PUBLIC
	exlock_now()        { _lock xn; }  # obtain an exclusive lock immediately or fail
	exlock()            { _lock x; }   # obtain an exclusive lock
	shlock()            { _lock s; }   # obtain a shared lock
	unlock()            { _lock u; }   # drop a lock

`
}

interface Options {
	uuid: string;
	quality: string;
	commit: string;
	enableTelemetry: boolean;
	extensionsSegment?: string;
	serverDataFolderName: string;
}

export const serverInstallScript = ({ uuid, quality, commit: commitID, enableTelemetry, extensionsSegment = "", serverDataFolderName }: Options) => {

	return `
COMMIT_ID="${commitID}"
EXTENSIONS="${extensionsSegment}"
TELEMETRY="${enableTelemetry ? "" : "--disable-telemetry"}"
export VSCODE_AGENT_FOLDER=$HOME/${serverDataFolderName}

#setup
VSCH_DIR="$VSCODE_AGENT_FOLDER/bin/$COMMIT_ID"
VSCH_LOGFILE="$VSCODE_AGENT_FOLDER/.$COMMIT_ID.log"

if [ ! -d "$VSCH_DIR" ]; then
	mkdir -p $VSCH_DIR
fi

${createlockFileScript(commitID)}
_prepare_locking

exlock_now
if (( $? > 0 ))
then
	echo "Installation already in progress..."
	echo "${uuid}##24##"
	exit 0
fi

# Migrate old data if needed
# Check data/ because we just created VSCH_DIR for the lockfile
if [ ! -d "$VSCODE_AGENT_FOLDER/data" ]; then
	if [ -d "$HOME/.vscode-remote" ]; then
		echo "Migrating .vscode-remote to $VSCODE_AGENT_FOLDER..."
		cp -r "$HOME/.vscode-remote/data" "$VSCODE_AGENT_FOLDER/data"
		cp -r "$HOME/.vscode-remote/extensions" "$VSCODE_AGENT_FOLDER/extensions"
	fi
fi

# install if needed
if [ ! -f "$VSCH_DIR/server.sh" ]
then
	echo "Installing to $VSCH_DIR..."
	STASHED_WORKING_DIR="\`pwd\`"
	cd $VSCH_DIR

	wget --version &> /dev/null
	if (( $? == 0 ))
	then
		echo "Downloading with wget"
		WGET_ERRORS=$(2>&1 wget -nv -O vscode-server-linux-x64.tar.gz https://update.code.visualstudio.com/commit:$COMMIT_ID/server-linux-x64/${quality})
		if (( $? != 0 ))
		then
			echo $WGET_ERRORS
			echo "${uuid}##25##"
			exit 0
		fi
	else
		curl --version &> /dev/null
		if (( $? == 0 ))
		then
			echo "Downloading with curl"
			CURL_OUTPUT=$(2>&1 curl -L -s -S https://update.code.visualstudio.com/commit:$COMMIT_ID/server-linux-x64/${quality} --output vscode-server-linux-x64.tar.gz -w "%{http_code}")
			if [[ ($? != 0) || ($CURL_OUTPUT != 2??) ]]
			then
				echo $CURL_OUTPUT
				echo "${uuid}##25##"
				exit 0
			fi
		else
			echo "Neither wget nor curl is installed"
			echo "${uuid}##26##"
			exit 0
		fi
	fi


	tar -xf vscode-server-linux-x64.tar.gz --strip-components 1
	if (( $? > 0 ))
	then
		echo "WARNING: tar exited with non-0 exit code"
	fi

	# cheap sanity check
	if [ ! -f $VSCH_DIR/node ]
	then
		echo "WARNING: $VSCH_DIR/node doesn't exist. Download/untar may have failed."
	fi

	if [ ! -f $VSCH_DIR/server.sh ]
	then
		echo "WARNING: $VSCH_DIR/server.sh doesn't exist. Download/untar may have failed."
	fi

	rm vscode-server-linux-x64.tar.gz
	cd $STASHED_WORKING_DIR

	# install extensions
	if [ ! -z "$EXTENSIONS" ]
	then
		echo "Installing extensions..."
		$VSCH_DIR/server.sh $TELEMETRY $EXTENSIONS
	fi

else
	echo "Found existing installation at $VSCH_DIR..."
fi

# launch if needed
RUNNING="\`ps ax | grep $VSCH_DIR/server.sh | grep -v grep | wc -l | tr -d '[:space:]'\`"
if [ "$RUNNING" = "0" ]
then
	echo "Printing the current remote environment..."
	printenv
	echo "Starting server..."
	export PATH="$VSCH_DIR/bin:$PATH"
	$VSCH_DIR/server.sh $TELEMETRY --port=0 &> $VSCH_LOGFILE < /dev/null &

	stopTime=$((SECONDS+4))

	while (($SECONDS < $stopTime))
	do
		PORT=\`cat "$VSCH_LOGFILE" | grep -a -E 'Extension host agent listening on [0-9]+' | grep -v grep | grep -o -E '[0-9]+'\`
		if [[ $PORT != '' ]]
		then
			break
		fi

		echo "Waiting for server log..."
		sleep .5
	done
else
	echo "Found running server..."
fi

echo " "
echo "*"
echo "* Reminder: You may only use this software with Visual Studio family products,"
echo "* as described in the license (https://go.microsoft.com/fwlink/?linkid=2077057)"
echo "*"
echo " "

PORT=\`cat "$VSCH_LOGFILE" | grep -a -E 'Extension host agent listening on [0-9]+' | grep -v grep | grep -o -E '[0-9]+'\`
if [[ $PORT == '' ]]
then
	echo "Server did not start successfully. Full server log:"
	cat $VSCH_LOGFILE
fi
echo "${uuid}==$PORT=="

unlock
`
}

export default {
	serverInstallScript,
}
