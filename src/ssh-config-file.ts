
import fs from "fs"
import os from "os"
import path from "path"
import util from "util"
import vscode from "vscode"
import SSHConfig from "ssh-config"

import platform from "./platform"
import dirUtils from "./dir-utils"
import Logger from "./logger"
import SshConfigFilePicker from "./ssh-config-file-picker"

// @ts-ignore
import package_json from "../package.json"
import { HostInfo } from "./host-info";

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)
const fileExists = util.promisify(fs.exists)
const chmod = util.promisify(fs.chmod)

const configFileKey = "remote.SSH.configFile"

export const sshConfigurationFileFromSetting = (): string => {
    return vscode.workspace.getConfiguration().get(configFileKey);
}

const sshConfigFilePath0 = path.resolve(os.userInfo().homedir, ".ssh/config")

const sshConfigFilePath1 = platform.isWindows
    ? "C:\\ProgramData\\ssh\\ssh_config"
    : "/etc/ssh/ssh_config";

export const defaultSshConfigurationFiles = () => {
    return [sshConfigFilePath0, sshConfigFilePath1];
}

export const getSshConfigurationFiles = (): string[] => {
    const configFileFromSetting = sshConfigurationFileFromSetting()

    if (configFileFromSetting) {
        return [configFileFromSetting]
    } else {
        return defaultSshConfigurationFiles()
    }
}

interface SSHConfigItem {
    param: string,
    value: string,
    config: SSHConfigItem[]
    [x: string]: any;
}

interface HostNames {  // map host -> hostname
    [host: string]: string  // hostname
}

const includeGlob = (host: string) => {
    return !!host.match(/[?*]/)
}

/**
 * @example
 * parseMultiHosts("github.com")  // returns ["github.com"]
 * 
 * parseMultiHosts("xxx.com example.com # comments")  // returns ["xxx.com", "example.com"]
 * 
 */
const parseMultiHosts = (hostsStr: string) => {
    return hostsStr
        .split("#")[0]                 // trim comments
        .split(" ")                    // split
        .filter(x => !!x)              // is not empty
        .filter(x => !includeGlob(x))  // does not include glob pattern
}

const extractHostNames = (sshConfig: SSHConfig): HostNames => {
    const result = Object.create(null)

    for (let i = 0; i < sshConfig.length; i++) {
        const item: SSHConfigItem = sshConfig[i];
        if (
            item.param && item.param.toLowerCase() === "host" &&
            item.value &&
            item.config
        ) {
            for (const x of item.config) {
                if (x.param && x.param.toLowerCase() === "hostname" && x.value) {

                    const hostname = x.value

                    parseMultiHosts(item.value).forEach(host => {
                        result[host] = hostname
                    })

                }
            }
        }
    }

    return result;
}

export const _getConfigurationForHost = async (hostinfo: HostInfo, sshConfigFilePaths: string[]) => {
    for (let configFilePath of sshConfigFilePaths) {

        const sshConfig = await readSshConfig(configFilePath)
        const t = sshConfig.compute(hostinfo.hostName)

        if (t && t.HostName) {

            if (t.IdentityFile) {
                t.IdentityFile = t.IdentityFile.map((p: string) => {
                    return dirUtils.untildify(p)
                })
            }

            return t
        }
    }

    return null;
}

const readFileContent = async (filePath: string) => {
    if (await fileExists(filePath)) {
        const data = await readFile(filePath)
        return data.toString("utf8")
    }

    return "";
}

const readSshConfig = async (filePath: string) => {
    const file = await readFileContent(filePath)
    return SSHConfig.parse(file)
}

const extractHosts = (sshConfig: SSHConfig) => {
    const hosts: Set<string> = new Set()

    for (let i = 0; i < sshConfig.length; i++) {
        const item = sshConfig[i]

        if (
            item.param && item.param.toLowerCase() == "host" &&
            item.value &&
            item.config
        ) {

            parseMultiHosts(item.value).forEach(host => {
                hosts.add(host)
            })

        }
    }

    return Array.from(hosts.keys())
}

export const getConfiguredSshHosts = async () => {
    const hosts: string[] = []

    for (let configFilePath of getSshConfigurationFiles()) {
        const sshConfig = await readSshConfig(configFilePath)
        hosts.push(...extractHosts(sshConfig))
    }

    return hosts
}

export const getConfiguredSshHostNames = async (): Promise<HostNames> => {
    const result = {}

    for (let configFilePath of getSshConfigurationFiles()) {
        const sshConfig = await readSshConfig(configFilePath)
        const hostNames = extractHostNames(sshConfig)

        Object.keys(hostNames).forEach(t => {
            result[t] = hostNames[t]
        })
    }

    return result
}

export const getConfigurationForHost = async function (hostinfo: HostInfo) {
    return _getConfigurationForHost(hostinfo, await getSshConfigurationFiles())
}

const configChangeEventEmitter = new vscode.EventEmitter()
const configChangeEvent = configChangeEventEmitter.event
export const onDidChangeSshConfiguration = configChangeEvent

vscode.workspace.onDidChangeConfiguration(e => {
    if (e.affectsConfiguration(configFileKey)) {
        configChangeEventEmitter.fire()
    }
})

class FileWatcher {
    logger: Logger

    fileBeingWatched: Set<string>

    saveListener: vscode.Disposable

    closeListener: vscode.Disposable

    constructor(logger: Logger) {
        this.logger = logger
        this.fileBeingWatched = new Set()
    }

    startWatching(url: vscode.Uri) {
        this.logger.trace(
            `ConfigFileWatcher: start watchting ${url.toString()}`
        )

        this.fileBeingWatched.add(url.toString())

        if (!this.saveListener) {
            this.logger.trace("ConfigFileWatcher: adding save listener")

            this.saveListener = vscode.workspace.onDidSaveTextDocument(document => {
                this.documentSaved(document)
            })
        }

        if (!this.closeListener) {
            this.logger.trace("ConfigFileWatcher: adding close listener")

            this.closeListener = vscode.workspace.onDidCloseTextDocument(document => {
                this.documentClosed(document)
            })
        }
    }

    stopWatching(url: vscode.Uri) {
        this.logger.trace(
            `ConfigFileWatcher: stop watchting ${url.toString()}`
        )

        this.fileBeingWatched.delete(url.toString())

        if (this.fileBeingWatched.size == 0) {
            if (this.saveListener) {
                this.logger.trace("ConfigFileWatcher: removing save listener")
                this.saveListener.dispose()
                this.saveListener = undefined
            }

            if (this.closeListener) {
                this.logger.trace("ConfigFileWatcher: removing close listener")
                this.closeListener.dispose()
                this.closeListener = undefined
            }
        }
    }

    documentSaved(document: vscode.TextDocument) {
        this.logger.trace(`ConfigFileWatcher: saving ${document.uri}`)

        if (this.fileBeingWatched.has(document.uri.toString())) {
            this.logger.trace(`ConfigFileWatcher: firing change event for ${document.uri}`)
            configChangeEventEmitter.fire()
        }
    }

    documentClosed(document: vscode.TextDocument) {
        this.logger.trace(`ConfigFileWatcher: closing ${document.uri}`)

        if (this.fileBeingWatched.has(document.uri.toString())) {
            this.stopWatching(document.uri)
        }
    }
}

const openSettings = async () => {
    const { name, publisher, extensionPack } = package_json

    const extensionIDs = [`${publisher}.${name}`]

    if (extensionPack) {
        extensionPack.forEach((id: string) => {
            extensionIDs.push(id)
        })
    }

    await vscode.commands.executeCommand(
        "workbench.action.openSettings",
        `@ext:${extensionIDs.join(",")} config file`
    )
}

const createSshConfigFile = async (filePath: string) => {
    const dirname = path.dirname(filePath)

    if (!await fileExists(dirname)) {
        await dirUtils.mkdirp(dirname)
        if (!platform.isWindows) {
            if (filePath === sshConfigFilePath0) {
                await chmod(dirname, 0o700)
            }
        }
    }

    await writeFile(filePath, "")

    if (!platform.isWindows) {
        if (filePath === sshConfigFilePath0) {
            await chmod(filePath, 0o600)
        }
    }

    const editor = await vscode.window.showTextDocument(vscode.Uri.file(filePath))

    await editor.insertSnippet(
        new vscode.SnippetString(
            "# Read more about SSH config files: https://linux.die.net/man/5/ssh_config\nHost ${1:alias}\n    HostName ${2:hostname}\n    User ${3:user}"
        )
    )
}

let filewatcher: FileWatcher;

export const configureSshHosts = async (logger: Logger) => {
    const sshConfigurationFile = sshConfigurationFileFromSetting()

    const result = await SshConfigFilePicker.pickSshConfigurationFile(sshConfigurationFile || defaultSshConfigurationFiles())

    if (result) {
        if (result.type === "command") {
            if (result.id === "help") {
                vscode.env.openExternal(
                    vscode.Uri.parse("https://aka.ms/vscode-remote/ssh/config-file")
                )
            } else if (result.id === "settings") {
                openSettings()
            }
        } else if (result.type === "file") {
            const filePath: string = result.path

            if (filePath) {
                const url = vscode.Uri.file(filePath)

                if (!filewatcher) {
                    filewatcher = new FileWatcher(logger)
                }

                filewatcher.startWatching(url)

                if (await fileExists(filePath)) {
                    await vscode.commands.executeCommand("vscode.open", url)
                } else {
                    await createSshConfigFile(filePath)
                }
            }
        }
    }
}

export default {
    defaultSshConfigurationFiles,
    getSshConfigurationFiles,
    getConfiguredSshHosts,
    getConfiguredSshHostNames,
    _getConfigurationForHost,
    getConfigurationForHost,
    sshConfigurationFileFromSetting,
    configureSshHosts,
    onDidChangeSshConfiguration
}
