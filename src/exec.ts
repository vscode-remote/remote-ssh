
import childProcess from "child_process"

type Options = { encoding: string } & childProcess.ExecOptions

const exec = async (command: string, options: Options = { encoding: "utf8" }) => {
    return new Promise((resolve, reject) => {
        childProcess.exec(command, options, (err, stdout, stderr) => {
            if (err) {
                return reject(err)
            }
            resolve(stdout)
        })
    })
}

export default {
    exec,
}
