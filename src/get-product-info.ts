import fs from "fs"
import path from "path"
import util from "util"
import vscode from "vscode"

const readFile = util.promisify(fs.readFile)

let product;

const getProduct = async () => {
    if (!product) {
        const filePath = path.join(vscode.env.appRoot, "product.json")
        const data = await readFile(filePath, "utf8")
        product = JSON.parse(data);
    }

    return product;
}

interface ProductInfo {
    commit: string;
    quality: string;
    serverDataFolderName: string;
}

export const getProductInfo = async (): Promise<ProductInfo> => {
    let productInfo: ProductInfo = await getProduct()

    if (!(productInfo.commit && productInfo.serverDataFolderName)) {
        productInfo = {
            commit: "94f7de61ece171f4390ece488a220d22cbb0fddd",
            quality: "insider",
            serverDataFolderName: ".vscode-server-oss"
        }
    }

    return productInfo
}

export default {
    getProductInfo,
}
