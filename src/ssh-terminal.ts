
import vscode from "vscode"
import Login, { LoginResult } from "./login"
import SshConfigFile from "./ssh-config-file"
import SshAgent from "./ssh-agent"
import platform from "./platform"
import StringUtils from "./string-utils"
import DisposeUtils from "./dispose-utils"
import { SystemInteractorType } from "./system-interactor"
import Logger from "./logger";
import { HostInfo } from "./host-info";

const showAskPasswordInputBox = async (title: string): Promise<string> => {
    return new Promise((resolve, reject) => {

        const inputbox = vscode.window.createInputBox()
        inputbox.title = title
        inputbox.password = true

        let accepted = false

        inputbox.onDidAccept(() => {
            accepted = true
            const value = inputbox.value
            inputbox.dispose()
            resolve(value)  // result
        })

        inputbox.onDidHide(() => {
            if (!accepted) {
                resolve(undefined)
            }
        })

        inputbox.show()

    })
}

const askForPassphrase = (sshKeyName?: string) => {
    return showAskPasswordInputBox(`Enter passphrase for ssh key ${sshKeyName ? `"${sshKeyName}"` : ""}`)
}

class ConfirmationQuickPickItem implements vscode.QuickPickItem {  // picker
    label: string;

    value: string;

    isCloseAffordance: boolean;

    title: string;

    constructor(title: string, value: string) {
        this.label = title
        this.value = value
        this.isCloseAffordance = true
        this.title = title
    }
}

export const showHostKeyConfirmation = async (hostName: string, fingerprintData: string): Promise<"yes" | "no" | string> => {
    return new Promise((resolve, reject) => {
        const quickpick: vscode.QuickPick<ConfirmationQuickPickItem> = vscode.window.createQuickPick()

        quickpick.canSelectMany = false
        quickpick.items = [
            new ConfirmationQuickPickItem("Continue", "yes"),
            new ConfirmationQuickPickItem("Cancel", "no")
        ]
        quickpick.title = `"${hostName}" has fingerprint "${fingerprintData}".`

        let accepted = false

        quickpick.onDidAccept(async () => {
            accepted = true
            const value = quickpick.selectedItems[0].value
            quickpick.dispose()
            resolve(value)
        })

        quickpick.onDidHide(() => {
            if (!accepted) {
                resolve()
            }
        })

        quickpick.show()
    })
}

interface InteractorType {
    onData(data: string): Promise<LoginResult>
}

interface RunCommandOptions {
    systemInteractor: SystemInteractorType;
    command: string;
    sendText?: string;
    interactor?: InteractorType;
    nickname: string;
    logger: Logger;
    token?: vscode.CancellationToken;
    revealTerminal?: vscode.Event<never>;
    host?: HostInfo;
}

const traceData = (data: string, nickname: string, logger: Logger) => {
    const _data = data.replace(/\r?\n$/, "")

    if (vscode.env.logLevel === vscode.LogLevel.Trace) {
        logger.trace(`"${nickname}" terminal received data: "${_data}"`)
    } else {
        const message = _data
            .split(/\n/)
            .map(line => `> ${line}`)
            .join("\n")

        logger.trace(message)
    }
}

export const runInteractiveSshTerminalCommand = (options: RunCommandOptions): Promise<string> => {
    const t = []

    let { systemInteractor, command, interactor, nickname, logger, token } = options

    return new Promise((resolve, reject) => {

        let closeTerminalDisposable: vscode.Disposable
        let writeDataDisposable: vscode.Disposable
        let terminal: vscode.Terminal
        let interval: NodeJS.Timeout
        let cancellationRequestedDisposable: vscode.Disposable
        let allData = "";

        const releaseAllResources = () => {
            if (writeDataDisposable) {
                writeDataDisposable.dispose()
                writeDataDisposable = undefined
            }

            if (terminal) {
                terminal.dispose()
                terminal = undefined
            }

            if (closeTerminalDisposable) {
                closeTerminalDisposable.dispose()
                closeTerminalDisposable = undefined
            }

            if (interval) {
                clearInterval(interval)
                interval = undefined
            }

            if (cancellationRequestedDisposable) {
                cancellationRequestedDisposable.dispose()
            }

            if (DisposeUtils) {
                DisposeUtils.dispose(t)
            }
        }

        const closeTerminal = (canceled = false) => {
            releaseAllResources()

            logger.debug(`"${nickname}" terminal command ${canceled ? "canceled" : "done"}`)

            const _data = canceled ? "" : StringUtils.lastNonemptyLine(allData)

            resolve(_data || "");
        }

        if (token) {
            cancellationRequestedDisposable = token.onCancellationRequested(() => {
                closeTerminal(true)
            })
        }

        try {
            let shellArgs: string | string[]

            if (options.sendText) {
                shellArgs = ""
            } else {
                shellArgs = platform.isWindows ? `/c (${command}) & exit /b 0` : ["-c", command + "\nexit 0"]
            }

            const terminalOptions: vscode.TerminalOptions = {
                cwd: platform.isWindows
                    ? vscode.Uri.file(process.env.USERPROFILE || "c:\\")
                    : vscode.Uri.file(process.env.HOME || "/"),
                name: nickname,
                shellPath: platform.isWindows ? "cmd.exe" : "sh",
                shellArgs: shellArgs
            }

            terminal = systemInteractor.createTerminal(terminalOptions)

            if (options.revealTerminal) {
                t.push(
                    options.revealTerminal(() => {
                        if (terminal) {
                            terminal.show()
                        }
                    })
                )
            }

            if (options.sendText) {

                const text = platform.isWindows
                    ? `(${options.sendText}) & exit /b 0`
                    : options.sendText + "\nexit 0"

                terminal.sendText(text)

                logger.trace(`"${nickname}" wrote data to terminal: "${options.sendText}"`)

            }

            const showLoginTerminal: boolean = vscode.workspace
                .getConfiguration()
                .get("remote.SSH.showLoginTerminal")

            if (showLoginTerminal) {
                interactor = undefined
                terminal.show()
            }

            closeTerminalDisposable = systemInteractor.onDidCloseTerminal((_terminal) => {
                if (_terminal === terminal) {
                    terminal = undefined
                    closeTerminal()
                }
            })

            writeDataDisposable = terminal.onDidWriteData(async (data: string) => {

                if (interval) {
                    clearInterval(interval)
                    interval = undefined
                }

                traceData(data, nickname, logger)

                allData += data

                if (interactor) {
                    const result = await interactor.onData(allData)

                    if (result.postAction === "consume") {
                        allData = ""
                    }

                    if (result.canceled) {
                        closeTerminal(true)
                        return
                    }

                    if (result.response) {
                        terminal.sendText(`${result.response}\n`)

                        const response = result.isPassword
                            ? result.response.replace(/./g, "*")
                            : result.response

                        logger.trace(`"${nickname}" wrote data to terminal: "${response}"`);
                    }
                }

            })

        } catch (err) {
            releaseAllResources()
            logger.error(`"${nickname}" process failed: ${err}`)
            reject(err)
        }
    })
}

export const runSshTerminalCommandWithLogin = async (options: RunCommandOptions) => {
    let passphrase: string

    const getPassphrase = async (sshKeyName?: string) => {
        passphrase = await askForPassphrase(sshKeyName)
        return passphrase
    }

    class Interactor implements InteractorType {
        async onData(data: string) {
            let result = await Login.handleFingerprint(data, options.host, showHostKeyConfirmation, options.logger)

            if (result.postAction !== "consume") {
                result = await Login.handlePassphrase(data, getPassphrase, options.logger)
            }

            return result
        }
    }

    const interactor = new Interactor()

    const result = await runInteractiveSshTerminalCommand({
        systemInteractor: options.systemInteractor,
        command: options.command,
        interactor,
        nickname: options.nickname,
        logger: options.logger,
        token: options.token,
        revealTerminal: options.revealTerminal
    })

    if (passphrase) {
        options.logger.trace("User entered passphrase")

        const sshConfig = await SshConfigFile.getConfigurationForHost(options.host)

        options.logger.trace("Passphrase was entered and ssh config entry was found - adding SSH key to agent")

        let identityFile: string = undefined
        if (sshConfig && sshConfig.IdentityFile) {
            identityFile = sshConfig.IdentityFile[0]
        }

        await SshAgent.addKeyToSshAgent(identityFile, async () => passphrase, options.logger)
    }

    return result
}

export default {
    showHostKeyConfirmation,
    runInteractiveSshTerminalCommand,
    runSshTerminalCommandWithLogin,
}
