import vscode from "vscode"

class FilePicker implements vscode.QuickPickItem {
    label: string;

    constructor(configFilePath: string) {
        this.label = configFilePath
    }
}

class HelpCommandPicker implements vscode.QuickPickItem {
    label: string;

    description: string;

    alwaysShow: boolean;

    constructor() {
        this.label = "Help"
        this.description = "about SSH configuration files"
        this.alwaysShow = true
    }
}

class SettingsCommandPicker implements vscode.QuickPickItem {
    label: string;

    description: string;

    alwaysShow: boolean;

    constructor() {
        this.label = "Settings"
        this.description = "specify a custom configuration file"
        this.alwaysShow = true
    }
}

interface CommandPickResult {
    type: "command";
    id: "help" | "settings";
}

interface FilePickResult {
    type: "file";
    path: string;
}

type PickResult = FilePickResult | CommandPickResult;

const buildQuickPick = (sshConfigFiles: string | string[]) => {
    const quickpick = vscode.window.createQuickPick()

    quickpick.ignoreFocusOut = true

    if (typeof sshConfigFiles == "string") {
        quickpick.title = "Edit custom SSH configuration file"
        quickpick.items = [
            new FilePicker(sshConfigFiles),
            new SettingsCommandPicker(),
            new HelpCommandPicker()
        ]
    } else {
        quickpick.title = "Select SSH configuration file to edit"
        quickpick.items = [
            ...sshConfigFiles.map((configFilePath: string) => new FilePicker(configFilePath)),
            new SettingsCommandPicker(),
            new HelpCommandPicker()
        ]
    }

    return quickpick
}

const _pickSshConfigFile = async (sshConfigFiles: string | string[]): Promise<PickResult> => {
    return new Promise((resolve, reject) => {

        const quickpick = buildQuickPick(sshConfigFiles)

        let result: PickResult

        quickpick.onDidAccept(() => {
            const item = quickpick.selectedItems[0]

            if (item instanceof HelpCommandPicker) {
                result = {
                    type: "command",
                    id: "help"
                }
            } else if (item instanceof SettingsCommandPicker) {
                result = {
                    type: "command",
                    id: "settings"
                }
            } else {
                if (item.label) {
                    result = {
                        type: "file",
                        path: item.label
                    }
                }
            }

            quickpick.hide()
        })

        quickpick.onDidHide(() => {
            resolve(result || undefined)
        })

        quickpick.show()
    })
}

const pickSshConfigurationFile = async (sshConfigFiles: string | string[]) => {
    return await _pickSshConfigFile(sshConfigFiles);
}

export default {
    pickSshConfigurationFile
}
