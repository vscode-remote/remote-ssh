import fs from "fs"
import path from "path"
import vscode from "vscode"
import platform from "./platform"
import SshConfigFile from "./ssh-config-file"

import { HostInfo } from "./host-info"
import Logger from "./logger";

export const getRemoteCommandSegment = () => {
    return "-o RemoteCommand=none"
}

export const getHostCommandSegment = (hostinfo: HostInfo) => {
    return hostinfo.user ? `${hostinfo.user}@${hostinfo.hostName}` : `${hostinfo.hostName}`;
}

export const getConnectTimeoutSegment = async (hostinfo: HostInfo) => {
    const config = await SshConfigFile.getConfigurationForHost(hostinfo)
    if (config && config.ConnectTimeout) {
        return ""
    } else {
        return "-o ConnectTimeout=15"
    }
}

export const getConfigFileCommandSegment = (logger: Logger) => {
    let flag = ""
    const configFile = vscode.workspace.getConfiguration().get("remote.SSH.configFile")
    if (configFile) {
        logger.info(`Using SSH config file "${configFile}"`)
        flag = `-F ${configFile}`
    }
    return flag
}

export const generateMultiLineCommand = async (hostinfo: HostInfo, command: string, logger: Logger): Promise<string> => {

    const host = getHostCommandSegment(hostinfo)

    if (platform.isWindows) {

        if (!process.env.TEMP) {
            throw new Error("The TEMP environment variable must be set")
        }

        const shFilePath = path.join(
            process.env.TEMP,
            `vscode-linux-multi-line-command-${hostinfo.hostName}.sh`
        )

        return new Promise((resolve) => {
            fs.writeFile(shFilePath, command, (err) => {

                if (err) {
                    throw new Error(`Failed to write install script to path ${shFilePath}. ${err.message}`)
                }

                resolve(`type "${shFilePath}" | ${getSshCommand()} ${getConfigFileCommandSegment(logger)} "${host}" bash`)

            })
        })

    } else {

        return `${getSshCommand()} ${await getConnectTimeoutSegment(hostinfo)} ${getConfigFileCommandSegment(logger)} "${host}" bash << 'EOSSH'\n${command}\nEOSSH`

    }
}

const isX64 = process.env.hasOwnProperty("PROCESSOR_ARCHITEW6432")
let sshCommand = "ssh"

if (platform.isWindows && isX64) {
    const systemRoot = process.env.SystemRoot || "C:\\WINDOWS"
    const sshExePath = path.join(systemRoot, "Sysnative", "OpenSSH", "ssh.exe")
    sshCommand = sshExePath
}

export const getSshCommand = () => {
    return sshCommand
}

export default {
    generateMultiLineCommand,
    getRemoteCommandSegment,
    getHostCommandSegment,
    getConnectTimeoutSegment,
    getConfigFileCommandSegment,
    getSshCommand,
}
