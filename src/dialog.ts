import vscode from "vscode"
import { HostInfo } from "./host-info"
import Logger from "./logger";

const getValidationMessage = (portStr: string) => {
    if (portStr !== "") {
        let port = parseInt(portStr)
        if (isNaN(port) || port <= 0) {
            return "Enter a valid port number"
        }
        if (port < 1024) {
            return "Port must be >= 1024"
        }
    }

    return "";
}

export const askForRemotePort = async (extensionContext: vscode.ExtensionContext, hostinfo: HostInfo, logger: Logger): Promise<number> => {
    let accepted = false

    return new Promise(async (resolve, reject) => {

        const inputbox = vscode.window.createInputBox()

        inputbox.title = `Enter the port on "${hostinfo.hostName}" that should be forwarded:`

        const savedPort = extensionContext.workspaceState.get("remote.SSH.tunnelPort")
        if (savedPort !== undefined) {
            inputbox.value = savedPort.toString()
        }

        inputbox.show()

        inputbox.onDidChangeValue((value) => {
            inputbox.validationMessage = getValidationMessage(value)
        })

        inputbox.onDidAccept(() => {
            if (!inputbox.validationMessage) {
                accepted = true

                const port = parseInt(inputbox.value)
                extensionContext.workspaceState.update("remote.SSH.tunnelPort", port)

                inputbox.hide()
                inputbox.dispose()

                resolve(port)
            }
        })

        inputbox.onDidHide(() => {
            if (!accepted) {
                resolve(null)
            }
        })

    })
}

const getDefaultForwarderNameByPort = (port: number) => {
    switch (port) {
        case 8000:
        case 80:
            return "Browser";

        case 5901:
            return "VNC";
    }

    return "";
}

export const askForForwarderName = async (hostinfo: HostInfo, port: number, logger: Logger): Promise<string> => {
    let accepted = false

    return new Promise(async (resolve, reject) => {

        const inputbox = vscode.window.createInputBox()

        inputbox.title = `Name the port forward for "${hostinfo.hostName}:${port}" or leave empty`

        inputbox.value = getDefaultForwarderNameByPort(port)

        inputbox.show()

        inputbox.onDidAccept(() => {
            if (!inputbox.validationMessage) {
                accepted = true
                inputbox.hide()
                inputbox.dispose()
                resolve(inputbox.value)
            }
        })

        inputbox.onDidHide(() => {
            if (!accepted) {
                resolve(null)
            }
        })

    })
}

export default {
    askForRemotePort,
    askForForwarderName,
}
