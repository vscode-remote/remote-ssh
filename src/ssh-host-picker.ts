
import vscode from "vscode"
import SshConfigFile from "./ssh-config-file"
import ConfirmHost from "./confirm-host"
import HostInfo from "./host-info"
import SshTerminal from "./ssh-terminal"
import SshAgent from "./ssh-agent"
import Logger from "./logger";

type HostInfoType = import("./host-info").HostInfo

class HostPicker implements vscode.QuickPickItem {
    label: string;

    constructor(host: string) {
        this.label = host;
    }
}

class ConfigureCommandPicker implements vscode.QuickPickItem {
    label: string;

    alwaysShow: boolean;

    constructor() {
        this.label = "Configure SSH Hosts..."
        this.alwaysShow = true
    }
}

class InputHostPicker implements vscode.QuickPickItem {
    hostName: string;

    alwaysShow: boolean;

    constructor() {
        this.hostName = ""
        this.alwaysShow = true
    }

    get label() {
        return `➤ ${this.hostName}`;
    }
}

const buildQuickPick = (hostList: string[]) => {

    const hostPickers = hostList.map(host => new HostPicker(host))

    const commandPicker = new ConfigureCommandPicker()

    const quickpick = vscode.window.createQuickPick()
    quickpick.ignoreFocusOut = true
    quickpick.title = "Select configured SSH host or enter user@host"

    const inputHostPicker = new InputHostPicker()
    let inputted = false

    const refreshQuickPick = () => {
        const items = [...hostPickers]
        if (inputted) {
            items.push(inputHostPicker)
        }
        items.push(commandPicker)
        quickpick.items = items
    };

    refreshQuickPick()

    quickpick.onDidChangeValue(inputStr => {
        inputHostPicker.hostName = inputStr

        if (typeof inputStr == "string" && inputStr.length > 0) {
            inputted = true
        } else {
            inputted = false
        }

        refreshQuickPick()
    })

    return quickpick
}

interface CommandPickResult {
    type: "command";
    id: "configure";
}

interface HostPickResult {
    type: "host";
    host: HostInfoType;
}

type PickResult = HostPickResult | CommandPickResult;

const getPickSshHostResult = async (hostList: string[], logger: Logger): Promise<PickResult> => {
    return new Promise((resolve, reject) => {

        const quickpick = buildQuickPick(hostList)

        let hostinfo: HostInfoType

        quickpick.onDidAccept(async () => {
            quickpick.busy = true

            const selectedItem = quickpick.selectedItems[0]

            if (selectedItem instanceof ConfigureCommandPicker) {
                resolve({
                    type: "command",
                    id: "configure"
                })
                return
            }

            const hostName = selectedItem instanceof InputHostPicker
                ? selectedItem.hostName
                : selectedItem.label

            hostinfo = HostInfo.HostInfo.fromString(hostName)

            logger.info(`Selected ${hostinfo.toString()}`)

            quickpick.busy = false
            quickpick.hide()
            quickpick.dispose()
        })

        quickpick.onDidHide(async () => {
            if (hostinfo) {
                const hostIsConnectable = await isConnectableSshHost(hostinfo, logger)

                if (hostIsConnectable) {
                    resolve({
                        type: "host",
                        host: hostinfo
                    })
                } else {
                    resolve(null)
                }
            }
        })

        quickpick.show()
    })
}

export const isConnectableSshHost = async (hostinfo:HostInfoType, logger:Logger) => {

    const options = {
        location: vscode.ProgressLocation.Notification,
        title: `Confirming that ${hostinfo} is reachable...`,
        cancellable: true,
    }

    const result = await vscode.window.withProgress(options, async (_, token) => {
        try {
            const confirmHostResult = await ConfirmHost.confirmHost(hostinfo, SshTerminal.runSshTerminalCommandWithLogin, logger, token)
            return confirmHostResult
        } catch (err) {
            if (err instanceof SshAgent.NoSshAgentError) {
                return await SshAgent.handleNoSshAgentError(true)
            } else {
                return false
            }
        }
    })

    return result

}

export const pickSshHost = async (logger: Logger) => {
    logger.info("Picking SSH host")
    const configuredHosts = await SshConfigFile.getConfiguredSshHosts()
    return await getPickSshHostResult(configuredHosts, logger);
}

export default {
    isConnectableSshHost,
    pickSshHost,
}
