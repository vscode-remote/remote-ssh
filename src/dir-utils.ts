
import os from "os"
import fs from "fs"
import path from "path"
import util from "util"
const mkdir = util.promisify(fs.mkdir)
const exists = util.promisify(fs.exists)

export const untildify = (filePath: string) => {
    return filePath.replace(/^~($|\/|\\)/, `${os.homedir()}$1`)
}

const maxDepth = 100

export const mkdirp = async (dirPath: string) => {
    let n = 0

    const _mkdir = async (p: string) => {
        const pathExists = await exists(p)

        if (n < maxDepth && !pathExists) {
            const s = path.dirname(p)

            if (s !== p) {
                n++
                await _mkdir(s)
            }

            await mkdir(p)
        }
    }

    return await _mkdir(dirPath)
}

export default {
    untildify,
    mkdirp,
}
