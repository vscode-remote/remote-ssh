
import vscode from "vscode"
import SshHostPicker from "./ssh-host-picker"
import Server from "./server"
import MultiLineCommand from "./multi-line-command"
import SshConfigFile from "./ssh-config-file"
import SshAgent from "./ssh-agent"
import Tunnels, { Tunnel } from "./tunnels"
import TunnelUtils from "./tunnel-utils"
import Exec from "./exec"
import HostInfo, { HostInfo as HostInfoType } from "./host-info"
import Logger from "./logger"
import GetProductInfo from "./get-product-info"

// @ts-ignore
import package_json from "../package.json"

type DisposableLike = { dispose(): any; }

const actionRetry = "Retry"
const actionIgnore = "Ignore"

const displayErrorMessage = async (errMsg: string) => {

    if (!errMsg.endsWith(".")) {
        errMsg += "."
    }

    const options = {
        modal: true
    }

    const actions = [
        {
            title: actionRetry
        },
        {
            title: actionIgnore,
            isCloseAffordance: true
        }
    ]

    const selectedAction = await vscode.window.showErrorMessage(
        errMsg,
        options,
        ...actions,
    )

    if (selectedAction && selectedAction.title === actionRetry) {
        await vscode.commands.executeCommand(
            "workbench.action.reloadWindow"
        )
    }

}

class SshRemoteAuthorityResolver implements vscode.RemoteAuthorityResolver {

    logger: Logger;

    disposables: DisposableLike[];

    constructor(logger: Logger, disposables: DisposableLike[]) {
        this.logger = logger
        this.disposables = disposables
    }

    async resolve(authority: string, context: vscode.RemoteAuthorityResolverContext) {

        this.logger.debug(`SSH Resolver called for "${authority}"`)

        const [protocol, hostinfoStr] = authority.split("+")

        if (protocol !== "ssh-remote") {
            throw new Error(`SSH Resolver called for invalid protocol "${protocol}"`);
        }

        const hostinfo = HostInfo.HostInfo.parse(hostinfoStr)

        this.logger.debug(`SSH Resolver called for host: ${hostinfo}`)

        let errMsg = `Could not establish connection to "${hostinfo.hostName}".`

        this.logger.info(`Setting up SSH remote "${hostinfo.hostName}"`);

        try {

            const options = {
                location: vscode.ProgressLocation.Notification,
                title: `Setting up SSH Host ${hostinfo.hostName}`,
                cancellable: false,
            }

            return await vscode.window.withProgress(options, (progress) => {
                return Server.resolve(authority, context, hostinfo, progress, this.logger, this.disposables)
            })

        } catch (err) {

            logger.error(err.message)

            if (err instanceof SshAgent.NoSshAgentError) {
                SshAgent.handleNoSshAgentError(false)
                throw vscode.RemoteAuthorityResolverError.NotAvailable(err.message, true)
            }

            if (context.resolveAttempt === 1) {

                logger.showOutputChannel()

                if (err.message) {
                    errMsg += " " + err.message
                }

                await displayErrorMessage(errMsg)
            }

            throw err
        }

    }

}

let logger: Logger

const getCleanCommands = async (isDevBox?: boolean) => {

    const { serverDataFolderName } = await GetProductInfo.getProductInfo()

    return `
		kill -9 \`ps ax | grep "out/vs/server/main.js" | grep -v grep | awk '{print $1}'\`
		kill -9 \`ps ax | grep "remoteExtensionHostAgent.js" | grep -v grep | awk '{print $1}'\`
		sleep 3
		# go after the restarted agent
		kill -9 \`ps ax | grep "out/vs/server/main.js" | grep -v grep | awk '{print $1}'\`
		kill -9 \`ps ax | grep "remoteExtensionHostAgent.js" | grep -v grep | awk '{print $1}'\`
		kill -9 \`ps ax | grep "watcherService" | grep -v grep | awk '{print $1}'\`
		rm -rf $HOME/${serverDataFolderName}/${isDevBox ? "" : "bin"}
		rm -rf $HOME/.vscode-remote/${isDevBox ? "" : "bin"}
    `
}

const isConnectableSshHost = (hostinfo: HostInfoType) => {
    hostinfo = new HostInfo.HostInfo(hostinfo.hostName, hostinfo.user)
    return SshHostPicker.isConnectableSshHost(hostinfo, logger)
}

const reportIssue = () => {

    const { name, publisher } = package_json

    return vscode.commands.executeCommand(
        "vscode.openIssueReporter",
        `${publisher}.${name}`
    )

}

const provideFeedback = () => {
    return vscode.env.openExternal(
        vscode.Uri.parse("https://aka.ms/vscode-remote/ssh/provide-feedback")
    );
}

const getStarted = () => {
    return vscode.env.openExternal(
        vscode.Uri.parse("https://aka.ms/vscode-remote/ssh/getting-started")
    )
}

const getHelp = () => {
    return vscode.env.openExternal(
        vscode.Uri.parse("https://aka.ms/vscode-remote/ssh")
    )
}

const openSettings = () => {
    const { name, publisher, extensionPack } = package_json

    const extensionIDs = [`${publisher}.${name}`]

    if (extensionPack) {
        extensionPack.forEach((id: string) => extensionIDs.push(id))
    }

    return vscode.commands.executeCommand(
        "workbench.action.openSettings",
        `@ext:${extensionIDs.join(",")}`
    )

}

const getRemoteAuthority = (hostinfo: HostInfoType) => {
    return `ssh-remote+${HostInfo.HostInfo.stringify(hostinfo)}`
}

const pickSshHost = async () => {

    const pickResult = await SshHostPicker.pickSshHost(logger);

    if (pickResult) {
        if ("command" === pickResult.type && "configure" === pickResult.id) {
            SshConfigFile.configureSshHosts(logger)
            return null
        }
        if ("host" === pickResult.type) {
            return pickResult.host
        }
    }

    return null

}

const openEmptyWindowOnSshHost = async (hostinfo: HostInfoType, reuseWindow = false) => {
    await vscode.commands.executeCommand("vscode.newWindow", {
        remoteAuthority: getRemoteAuthority(hostinfo),
        reuseWindow,
    })
}

const printExtensionVersion = (logger: Logger) => {
    const { name: t, version: o } = package_json
    logger.info(`${t}@${o}`)
}

export const activate = (context: vscode.ExtensionContext) => {

    const remoteSshNightlyInstalled = vscode.extensions.all.find(
        (extension) => {
            return "ms-vscode-remote.remote-ssh-nightly" === extension.id
        }
    )

    const remoteSshInstalled = vscode.extensions.all.find(
        (extension) => {
            return "ms-vscode-remote.remote-ssh" === extension.id
        }
    )

    if (remoteSshNightlyInstalled && remoteSshInstalled) {
        vscode.window.showErrorMessage(
            "Both remote-ssh and remote-ssh-nightly are installed. You must *uninstall*, not disable, the other remote extension.",
            {
                modal: true
            }
        )
    }

    logger = new Logger("Remote - SSH")

    context.subscriptions.push(vscode.Disposable.from(logger))

    const resolver = new SshRemoteAuthorityResolver(logger, context.subscriptions)

    vscode.workspace.registerRemoteAuthorityResolver("ssh-remote", resolver)

    printExtensionVersion(logger)

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.openEmptyWindow",
            async () => {
                const hostinfo = await pickSshHost()
                if (hostinfo) {
                    openEmptyWindowOnSshHost(hostinfo)
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.openEmptyWindowOnCurrentHost",
            async () => {
                const hostinfo = Server.getActiveRemote()
                if (hostinfo) {
                    openEmptyWindowOnSshHost(hostinfo)
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.openEmptyWindowInCurrentWindow",
            async () => {
                const hostinfo = await pickSshHost()
                if (hostinfo) {
                    openEmptyWindowOnSshHost(hostinfo, true)
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.openConfigFile",
            async () => {
                await SshConfigFile.configureSshHosts(logger)
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.cleanDevBox",
            async () => {
                const hostinfo = await pickSshHost()

                if (hostinfo) {
                    const cleanCommands = await getCleanCommands(true)
                    const command = await MultiLineCommand.generateMultiLineCommand(hostinfo, cleanCommands, logger)
                    await Exec.exec(command)
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.cleanRemoteServer",
            async () => {
                const hostinfo = await pickSshHost();

                if (hostinfo) {
                    const cleanCommands = await getCleanCommands()
                    const command = await MultiLineCommand.generateMultiLineCommand(hostinfo, cleanCommands, logger)
                    await Exec.exec(command)
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.manageTunnel",
            async () => {
                const activeRemote = Server.getActiveRemote()
                if (activeRemote) {
                    const address = await TunnelUtils.getTunnelAddress(context, activeRemote, logger)
                    return address
                } else {
                    return vscode.window.showWarningMessage(
                        "Port forwarding requires a workspace on a SSH host."
                    )
                }
            }
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.closeTunnel",
            async () => await TunnelUtils.closeTunnel(logger)
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand("opensshremotes.getHelp", async () =>
            getHelp()
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand("opensshremotes.getStarted", async () =>
            getStarted()
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand("opensshremotes.reportIssue", async () =>
            reportIssue()
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand("opensshremotes.settings", async () =>
            openSettings()
        )
    )

    context.subscriptions.push(
        vscode.commands.registerCommand(
            "opensshremotes.provideFeedback",
            async () => provideFeedback()
        )
    )

    return {
        getConfiguredSshHostNames: SshConfigFile.getConfiguredSshHostNames,
        configureSshHosts: () => SshConfigFile.configureSshHosts(logger),
        onDidChangeSshConfiguration: SshConfigFile.onDidChangeSshConfiguration,
        openEmptyWindowOnSshHost: openEmptyWindowOnSshHost,
        getActiveSshHost: Server.getActiveRemote,
        reportIssue: reportIssue,
        provideFeedback: provideFeedback,
        getHelp: getHelp,
        getStarted: getStarted,
        tunnels: {
            open: async () => {
                const activeRemote = Server.getActiveRemote()
                if (activeRemote) {
                    return TunnelUtils.openTunnel(context, activeRemote, false, logger)
                }
            },
            close: async (tunnel: Tunnel) => {
                await tunnel.close();
            },
            active: Tunnels.getActiveTunnels,
            onDidChange: Tunnels.onDidChangeActiveTunnels,
            description: TunnelUtils.tunnelDescription,
            label: TunnelUtils.tunnelLabel
        },
        isConnectableSshHost: isConnectableSshHost
    }

}

export const deactivate = () => {
    Server.shutdown()
}

export default {
    activate,
    deactivate,
}
