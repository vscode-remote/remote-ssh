
import ChildProcess, { ChildProcessWithoutNullStreams } from "child_process"
import vscode from "vscode"
import MultiLineCommand from "./multi-line-command"
import SshTerminal from "./ssh-terminal"
import Port from "./port"
import SystemInteractor from "./system-interactor"
import Wait from "./wait"
import Logger from "./logger";
import { HostInfo } from "./host-info";

export class Tunnel {

    host: HostInfo;

    remotePort: number;
    localPort: number;

    name: string;

    internal: boolean;

    constructor(hostinfo: HostInfo, remotePort: number, localPort: number, name: string, internal: boolean) {
        this.host = hostinfo
        this.remotePort = remotePort
        this.localPort = localPort
        this.name = name
        this.internal = internal
    }

    close() {
        _closeTunnel(this);
    }

    dispose() {

    }

}

class TunnelWithChildProcess extends Tunnel {

    process: ChildProcessWithoutNullStreams;

    constructor(hostinfo: HostInfo, remotePort: number, localPort: number, name: string, internal: boolean, process: ChildProcessWithoutNullStreams) {
        super(hostinfo, remotePort, localPort, name, internal)
        this.process = process
    }

    dispose() {
        if (this.process) {
            this.process.kill()
            this.process = null
        }
    }

}

class TunnelWithTokenSource extends Tunnel {

    tokenSource: vscode.CancellationTokenSource;

    constructor(hostinfo: HostInfo, remotePort: number, localPort: number, name: string, internal: boolean, tokenSource: vscode.CancellationTokenSource) {
        super(hostinfo, remotePort, localPort, name, internal)
        this.tokenSource = tokenSource
    }

    dispose() {
        this.tokenSource.cancel()
    }

}

type Progress = vscode.Progress<{ message?: string; increment?: number }>

export interface CreateTunnelOptions {
    host: HostInfo;
    remotePort: number;
    preferredLocalPort?: number;
    name: string;
    internal: boolean;
    progress: Progress;
    logger: Logger;
}

let allTunnels: Tunnel[] = [];

export const getActiveTunnels = () => {
    return allTunnels.filter(e => !e.internal);
};

let changeActiveTunnelsEventEmitter: vscode.EventEmitter<Tunnel> = new vscode.EventEmitter()

const _closeTunnel = (tunnel: Tunnel) => {
    allTunnels = allTunnels.filter(t => t !== tunnel)
    tunnel.dispose()
    changeActiveTunnelsEventEmitter.fire()
}

export const onDidChangeActiveTunnels = changeActiveTunnelsEventEmitter.event

export const closeTunnels = () => {  // closeAllTunnels
    allTunnels.forEach(tunnel => tunnel.dispose())
    allTunnels = []
    changeActiveTunnelsEventEmitter.fire()
}

const findPort = async (preferredLocalPort: number) => {
    let port = await Port.findFreePort(preferredLocalPort, 1, 3000)
    if (port > 0) {
        return port
    } else {
        return Port.findFreePort(Port.randomPort(), 10, 3000)
    }
}

const getSshConnectCommand = (hostinfo: HostInfo, remotePort: number, localPort: number, logger: Logger) => {

    const ssh = MultiLineCommand.getSshCommand()
    const flags = MultiLineCommand.getConfigFileCommandSegment(logger)
    const host = MultiLineCommand.getHostCommandSegment(hostinfo)

    const command = `${ssh} -t ${flags} "${host}" -L localhost:${localPort}:localhost:${remotePort} "echo -e 'Connected to SSH Host - Please do not close this terminal' && sleep infinity"`

    return command

}

const getSshConnectCommandObj = (hostinfo: HostInfo, remotePort: number, localPort: number, logger: Logger) => {

    const flags = MultiLineCommand.getConfigFileCommandSegment(logger)
    const host = MultiLineCommand.getHostCommandSegment(hostinfo)

    const argsList = [
        ...flags.split(" "),
        host,
        "-N",
        "-L",
        `localhost:${localPort}:localhost:${remotePort}`
    ].filter(e => !!e.trim())

    return {
        cmd: MultiLineCommand.getSshCommand(),
        argsList,
    }

}

const _createTunnel = (host: HostInfo, remotePort: number, localPort: number, name: string, internal: boolean, progress: Progress, logger: Logger) => {

    let tunnel: Tunnel;

    progress.report({
        message: "Setting up SSH tunnel"
    })

    const showLoginTerminal: boolean = vscode.workspace.getConfiguration().get("remote.SSH.showLoginTerminal")

    if (showLoginTerminal) {

        const command = getSshConnectCommand(host, remotePort, localPort, logger)

        logger.trace(`Spawning tunnel with: ${command}`)

        const tokenSource = new vscode.CancellationTokenSource()

        tunnel = new TunnelWithTokenSource(host, remotePort, localPort, name, internal, tokenSource)
        allTunnels.push(tunnel)

        SshTerminal
            .runSshTerminalCommandWithLogin({
                systemInteractor: SystemInteractor.defaultSystemInteractor,
                command: command,
                host: host,
                nickname: "SSH Tunnel",
                logger: logger,
                token: tokenSource.token
            })
            .then(
                () => {
                    logger.trace("SSH tunnel command completed unexpectedly")
                },
                (err) => {
                    const message = err && err.message
                    logger.trace("SSH tunnel command completed unexpectedly with error: " + message)
                }
            )
            .finally(() => {
                _closeTunnel(tunnel)
            })

    } else {

        const commandObj = getSshConnectCommandObj(host, remotePort, localPort, logger)

        logger.debug(`Spawning tunnel with: ${commandObj.cmd} ${commandObj.argsList.join(" ")}`)

        const process = ChildProcess.spawn(commandObj.cmd, commandObj.argsList, { detached: true })

        process.stderr.on("data", (data) => {
            logger.trace(`Tunnel(${remotePort}) stderr: ${data.toString()}`);
        })

        process.stdout.on("data", (data) => {
            logger.trace(`Tunnel(${remotePort}) stdout: ${data.toString()}`);
        })

        process.unref()

        tunnel = new TunnelWithChildProcess(host, remotePort, localPort, name, internal, process)
        allTunnels.push(tunnel)

        process.on("close", () => {
            _closeTunnel(tunnel)
        })

    }

    changeActiveTunnelsEventEmitter.fire(tunnel)

    logger.info(`Spawned SSH tunnel between local port ${localPort} and remote port ${remotePort}`)

}

const waitUntilPortIsNotFree = async (port: number, maxWaitTime = 10000) => {
    let timeWaited = 0;

    while (await Port.isFreePort(port)) {
        if (timeWaited > maxWaitTime) {
            throw new Error("Max wait time exhausted")
        }
        await Wait.wait(100)
        timeWaited += 100
    }
}

export const createTunnel = async ({ host, remotePort, preferredLocalPort, name, internal, progress, logger }: CreateTunnelOptions) => {
    try {

        const localPort = await findPort(preferredLocalPort || remotePort)

        await _createTunnel(host, remotePort, localPort, name, internal, progress, logger)

        progress.report({
            message: "Waiting for port forwarding to be ready"
        })

        logger.info("Waiting for ssh tunnel to be ready")

        await waitUntilPortIsNotFree(localPort)

        logger.info(`Tunneling remote port ${remotePort} to local port ${localPort}`)

        return localPort

    } catch (err) {
        const errMessage = err.message || err
        const message = `Could not establish tunnel to "${host.hostName}" (${errMessage}).`

        logger.error(message)

        throw new Error(message)
    }
}

export default {
    getActiveTunnels,
    onDidChangeActiveTunnels,
    closeTunnels,
    createTunnel,
}
