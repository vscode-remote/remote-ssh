
import platform from "./platform"

export const stripEscapeSequences = (str: string) => {
    return str
        .replace(/\x1b\[\??[0-9]{0,3}(;[0-9]{1,3})?[a-zA-Z]/g, "")
        .replace(/\r/g, "");
}

export const lastNonemptyLine = (str: string) => {
    const t = str.split("\n")

    if (platform.isWindows) {
        for (let i = t.length - 1; i >= 0; i--) {
            const s = stripEscapeSequences(t[i]);
            if (s) return s;
        }
    }

    const n = t.filter(e => !!e);
    return n[n.length - 1];
}

export default {
    stripEscapeSequences,
    lastNonemptyLine,
}
