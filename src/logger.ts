import vscode from "vscode"
import StringUtils from "./string-utils"

export class Logger {

    disposables: vscode.Disposable[];

    outputChannel: vscode.OutputChannel;

    constructor(name: string) {
        this.disposables = []

        this.outputChannel = vscode.window.createOutputChannel(name)

        this.disposables.push(this.outputChannel)

        this.disposables.push(
            vscode.env.onDidChangeLogLevel(() => this.logCurrentLogLevel())
        )
    }

    showOutputChannel() {
        return this.outputChannel.show();
    }

    info(message: string) {
        this.log(message, vscode.LogLevel.Info);
    }

    trace(message: string) {
        this.log(message, vscode.LogLevel.Trace);
    }

    debug(message: string) {
        this.log(message, vscode.LogLevel.Debug);
    }

    error(message: string) {
        this.log(message, vscode.LogLevel.Error);
    }

    logCurrentLogLevel() {
        this.info("Log Level: " + vscode.LogLevel[vscode.env.logLevel]);
    }

    log(message: string, level: vscode.LogLevel) {
        message = String(message)

        if (vscode.env.logLevel !== vscode.LogLevel.Trace) {
            message = StringUtils.stripEscapeSequences(message)
        }

        this.outputChannel.appendLine(message)
    }

    dispose() {
        this.outputChannel.dispose();
    }

}

export default Logger
