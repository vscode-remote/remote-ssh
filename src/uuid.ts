

class _UUID {
    _value: string

    constructor(value: string) {
        this._value = value;
    }

    asHex() {
        return this._value;
    }
}

class UUID extends _UUID {
    constructor() {
        super(
            [
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                "-",
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                "-",
                "4",
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                "-",
                UUID._oneOf(UUID._timeHighBits),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                "-",
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex(),
                UUID._randomHex()
            ].join("")
        );
    }

    static _oneOf(arr: any[]) {
        return arr[Math.floor(arr.length * Math.random())];
    }

    static _randomHex() {
        return UUID._oneOf(UUID._chars);
    }

    static get _chars() {
        return [
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "a",
            "b",
            "c",
            "d",
            "e",
            "f"
        ]
    }

    static get _timeHighBits() {
        return ["8", "9", "a", "b"]
    }
}

export const newUUID = () => {
    return new UUID();
}

const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

export const isUUID = (str: string) => {
    return uuidRegex.test(str);
}

export const parse = (uuidStr: string) => {
    if (!isUUID(uuidStr)) throw new Error("invalid uuid");
    return new _UUID(uuidStr)
}

export const generateUuid = () => {
    return newUUID().asHex();
}

export default {
    v4: newUUID,
    isUUID,
    parse,
    generateUuid,
}
