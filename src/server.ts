
import vscode from "vscode"
import login from "./login"
import ServerInstallScript from "./server-install-script"
import MultiLineCommand from "./multi-line-command"
import SshTerminal from "./ssh-terminal"
import Tunnels from "./tunnels"
import Port from "./port"
import GetProductInfo from "./get-product-info"
import SystemInteractor, { SystemInteractorType } from "./system-interactor"
import UUID from "./uuid"
import Wait from "./wait"
import Logger from "./logger";
import { HostInfo } from "./host-info";

const context = "remote.sshContext"

let activeRemote: HostInfo = null

const registerResourceLabelFormatter = (authority: string, hostinfo: HostInfo) => {
    return vscode.workspace.registerResourceLabelFormatter({
        scheme: "vscode-remote",
        authority: authority,
        formatting: {
            label: "${path}",
            separator: "/",
            workspaceSuffix: `SSH: ${hostinfo.hostName}`,
            tildify: true,
            normalizeDriveLetter: true,
        }
    })
}

export const resolve = async (authority: string, resolverContext: vscode.RemoteAuthorityResolverContext, hostinfo: HostInfo, progress: Progress, logger: Logger, disposables: vscode.Disposable[]) => {

    const systemInteractor = SystemInteractor.defaultSystemInteractor

    const remotePort = await installServer(systemInteractor, hostinfo, progress, logger)

    const localPort = await Tunnels.createTunnel({
        host: hostinfo,
        remotePort: remotePort,
        preferredLocalPort: Port.randomPort(),
        name: "VS Code internal",
        internal: true,
        progress: progress,
        logger: logger
    });

    logger.info(
        `Resolving "${authority}" to "localhost:${localPort}", attempt: ${resolverContext.resolveAttempt}`
    )

    vscode.commands.executeCommand("setContext", context, true)

    disposables.push(
        registerResourceLabelFormatter(authority, hostinfo)
    )

    activeRemote = hostinfo

    return new vscode.ResolvedAuthority("localhost", localPort)

}

const revealTerminalCommand = "opensshremotes.revealTerminal"

let revealTerminalEventEmitter: vscode.EventEmitter<never>

const getListeningPort = (data: string, uuid: string) => {
    const match = new RegExp(`${uuid}==([0-9]*)==`).exec(data);

    if (match) {
        const port = parseInt(match[1])
        if (Number.isNaN(port)) {
            return undefined
        } else {
            return port
        }
    }
}

type Progress = vscode.Progress<{ message?: string; increment?: number }>

const getServerInstallCommand = async (hostinfo: HostInfo, uuid: string, logger: Logger) => {

    const productinfo = await GetProductInfo.getProductInfo()

    let extensionsInstallationFlags: string;

    logger.info(
        `Using commit id "${productinfo.commit}" and quality "${productinfo.quality}" for server`
    )

    let defaultExtensions: string[] = vscode.workspace
        .getConfiguration()
        .get("remote.SSH.defaultExtensions")

    if (defaultExtensions && defaultExtensions.length > 0) {

        const extensionNameRegex = /^[a-z0-9][a-z0-9\-]*\.[a-z0-9][a-z0-9\-]*$/i

        defaultExtensions = defaultExtensions.filter(name => extensionNameRegex.test(name))

        if (defaultExtensions.length > 0) {

            extensionsInstallationFlags = defaultExtensions
                .map(extension => `--install-extension ${extension}`)
                .concat("--force")
                .join(" ")

        }

    }

    const enableTelemetry: boolean = vscode.workspace.getConfiguration().get("telemetry.enableTelemetry")

    const serverInstallScript = ServerInstallScript.serverInstallScript({
        uuid,
        serverDataFolderName: productinfo.serverDataFolderName,
        quality: productinfo.quality,
        commit: productinfo.commit,
        enableTelemetry: !!enableTelemetry,
        extensionsSegment: extensionsInstallationFlags
    })

    const command = MultiLineCommand.generateMultiLineCommand(hostinfo, serverInstallScript, logger)
    return command

}

/**
 * @returns {Promise<number>} remotePort
 */
export const installServer = async (systemInteractor: SystemInteractorType, hostinfo: HostInfo, progress: Progress, logger: Logger, uuid = UUID.generateUuid()): Promise<number> => {

    const command = await getServerInstallCommand(hostinfo, uuid, logger)

    logger.info("Install and start server if needed")

    revealTerminalEventEmitter = new vscode.EventEmitter()

    progress.report({
        message: `([details](command:${revealTerminalCommand} "Show details in terminal")) Initializing VS Code Server`
    })

    const _tryInstall = async () => {

        let retry = 0

        retryLoop:
        while (retry++ < 30) {

            const data = await SshTerminal.runSshTerminalCommandWithLogin({
                systemInteractor: systemInteractor,
                command: command,
                host: hostinfo,
                nickname: "install",
                logger: logger,
                revealTerminal: revealTerminalEventEmitter.event
            })

            logger.debug(`Received install output: ${data}`)

            const exitCode = login.getExitCode(data, uuid)

            if (typeof exitCode == "number") {

                if (exitCode === 0) {

                    if (data.match(/Network is unreachable/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "Network is unreachable"
                        )
                    }

                    if (data.match(/Could not resolve hostname/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "Could not resolve hostname"
                        )
                    }

                    if (data.match(/The process tried to write to a nonexistent pipe/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "The process tried to write to a nonexistent pipe"
                        )
                    }

                    if (data.match(/Connection timed out/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "The connection timed out"
                        )
                    }

                    if (data.match(/Operation timed out/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "The operation timed out"
                        )
                    }

                    if (data.match(/No route to host/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "No route to host"
                        )
                    }

                    if (data.match(/Can't assign requested address/)) {
                        throw vscode.RemoteAuthorityResolverError.TemporarilyNotAvailable(
                            "Can't assign requested address"
                        )
                    }

                    /** remotePort */
                    const listeningPort = getListeningPort(data, uuid)

                    if (typeof listeningPort == "number" && listeningPort > 0) {
                        logger.info(`Server is listening on port ${listeningPort}`)
                        return listeningPort
                    } else {
                        logger.error(`Failed to parse remote port from server output: ${data}`)
                        const err = vscode.RemoteAuthorityResolverError.NotAvailable("", true)
                        throw err
                    }

                }

                if (exitCode === 24) {

                    logger.error(
                        `Server installation process already in progress - waiting and retrying ${retry}`
                    )

                    progress.report({
                        message: `Connect to VS Code Server - retry ${retry}`
                    })

                    await Wait.wait(1000)

                    continue retryLoop

                }

                if (exitCode === 25) {

                    logger.error("Server download failed")

                    const err = vscode.RemoteAuthorityResolverError.NotAvailable(
                        "Downloading VS Code Server failed. Please try again later.",
                        true
                    )

                    throw err

                } else if (exitCode === 26) {

                    logger.error(
                        "Neither curl nor wget is installed - can't download the Server"
                    )

                    const err = vscode.RemoteAuthorityResolverError.NotAvailable(
                        "Downloading VS Code Server failed - please install either curl or wget on the remote.",
                        true
                    )

                    throw err
                } else {

                    logger.error(
                        `Server installation failed with exit code ${exitCode} and output ${data}`
                    )

                    throw vscode.RemoteAuthorityResolverError.NotAvailable("", true)
                }

            } else {

                logger.error(
                    `Server installation failed with unknown exit code and output ${data}`
                )

                throw vscode.RemoteAuthorityResolverError.NotAvailable("", true)

            }

        }

        throw vscode.RemoteAuthorityResolverError.NotAvailable("", true)

    }

    try {
        const remotePort = await _tryInstall()
        return remotePort
    } finally {
        revealTerminalEventEmitter.dispose()
    }

}

vscode.commands.registerCommand(revealTerminalCommand, () => {
    if (revealTerminalEventEmitter) {
        revealTerminalEventEmitter.fire()
    }
})

export const getActiveRemote = () => {
    return activeRemote
}

export const shutdown = () => {
    vscode.commands.executeCommand("setContext", context, false)
    activeRemote = null
    Tunnels.closeTunnels()
}

export default {
    resolve,
    installServer,
    getActiveRemote,
    shutdown,
}
