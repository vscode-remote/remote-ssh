import vscode from "vscode"
import MultiLineCommand from "./multi-line-command"
import SystemInteractor from "./system-interactor"
import Logger from "./logger";
import { HostInfo } from "./host-info";
import { runSshTerminalCommandWithLogin } from "./ssh-terminal";

type RunTerminalCommandFunction = typeof runSshTerminalCommandWithLogin;  // | typeof runInteractiveSshTerminalCommand

type Token = vscode.CancellationToken;

export const confirmHost = async (hostinfo: HostInfo, runTerminalCommandFunction: RunTerminalCommandFunction, logger: Logger, token: Token) => {

    const commandStr = await MultiLineCommand.generateMultiLineCommand(hostinfo, "uname -sm", logger)

    logger.info(`Confirming that ${hostinfo.toString()} is a valid reachable host`)
    logger.trace(`Running ${commandStr} to confirm the host platform`)

    const response = await runTerminalCommandFunction({
        systemInteractor: SystemInteractor.defaultSystemInteractor,
        command: commandStr,
        host: hostinfo,
        nickname: "uname",
        logger: logger,
        token: token
    })


    const isSuccessful = !!response.match(/linux x86_64/i)

    if (!isSuccessful && !(token && token.isCancellationRequested)) {

        vscode.window.showErrorMessage(
            `Can't connect to ${hostinfo.toString()}: unreachable or not Linux x86_64 (${response})`
        )

        logger.error(`${hostinfo.toString()}: unreachable or not Linux x86_64. (${response})`)

    }

    return isSuccessful
}

export default {
    confirmHost,
}
