
import fs from "fs"
import os from "os"
import path from "path"
import util from "util"
import vscode from "vscode"
import Login, { GetPassphraseFunction, LoginResult } from "./login"
import SshTerminal from "./ssh-terminal"
import platform from "./platform"
import SystemInteractor from "./system-interactor"
import UUID from "./uuid"
import Logger from "./logger"

const writeFile = util.promisify(fs.writeFile)

interface AddKeyResult {
    command: string;
    sendText?: string;
}

const addKeyWindows = async (identityFile: string, uuid: string) => {
    identityFile = identityFile || ""

    const tmpdir = os.tmpdir()
    const ps1FilePath = path.join(tmpdir, "vscode_add_ssh_key_to_agent.ps1")
    const content = `
		# Check if it's already in the ssh-agent
		$KEY = "${identityFile}"
		if ($KEY -ne "") {
			$KEY = (Resolve-Path "${identityFile}").toString()
		}

		echo 'PS: adding key to ssh-agent'
		echo ssh-add $KEY
		ssh-add $KEY
		if ($LASTEXITCODE -ne 0) {
			echo ${uuid}##5##
		}
	`;

    await writeFile(ps1FilePath, content)

    return {
        command: "",
        sendText: `powershell -ExecutionPolicy Bypass ${ps1FilePath}`
    }

}

const addKeyLinux = (identityFile: string, uuid: string) => {
    identityFile = identityFile || ""

    return {
        command: `
            INSTALL_SCRIPT=\`mktemp\`.sh
            cat << 'EOF' > $INSTALL_SCRIPT
                # expand the key path to be absolute
                KEY="\`echo ${identityFile}\`"
                echo SHELL-SCRIPT: KEY=$KEY
                echo SHELL-SCRIPT: adding key to ssh-agent
                ssh-add ${identityFile}
                if (( $? > 0 ))
                then
                    echo ${uuid}##5##
                fi
            EOF

            chmod +x $INSTALL_SCRIPT

            /bin/bash -ilc $INSTALL_SCRIPT
        `
    };
}

const addKey = async (identityFile: string, uuid: string): Promise<AddKeyResult> => {
    if (platform.isWindows) {
        return await addKeyWindows(identityFile, uuid)
    } else {
        return addKeyLinux(identityFile, uuid)
    }
}

export class NoSshAgentError extends Error {
    constructor() {
        super("Key with passphrase must be in ssh-agent");
    }
}

export const addKeyToSshAgent = async (identityFile: string, getPassphraseFunction: GetPassphraseFunction, logger: Logger) => {
    const uuid = UUID.generateUuid()
    const result = await addKey(identityFile, uuid)

    let exitCode: number

    const interactor = {
        async onData(data: string): Promise<LoginResult> {
            exitCode = Login.getExitCode(data, uuid)

            if (exitCode === undefined) {
                logger.error("Malformed exit code from add-ssh")
            } else if (exitCode === 0) {
                return Login.handlePassphrase(data, getPassphraseFunction, logger)
            } else {
                logger.error("add-ssh-key failed")
            }

            return {};
        }
    }

    logger.trace("Executing add-ssh-key:\n" + (result.sendText || result.command))

    await SshTerminal.runInteractiveSshTerminalCommand({
        systemInteractor: SystemInteractor.defaultSystemInteractor,
        command: result.command,
        sendText: result.sendText,
        interactor: interactor,
        nickname: "add-ssh-key",
        logger: logger
    })

    if (exitCode) {  // exitCode !== 0
        throw new NoSshAgentError()
    }

}

const configSshAgentActionStr = "Configure SSH Agent"
const continueWithoutSshAgentActionStr = "Continue without SSH Agent"

export const handleNoSshAgentError = async (canContinue: boolean) => {
    let message = "SSH Agent not found, please configure the agent."

    const actionItems = [configSshAgentActionStr]

    if (canContinue) {
        actionItems.push(continueWithoutSshAgentActionStr)
        message += " You can also continue without an SSH Agent with a degraded user experience."
    }

    const selectedAction = await vscode.window.showErrorMessage(message, { modal: true }, ...actionItems)

    if (selectedAction === continueWithoutSshAgentActionStr) {

        await vscode.workspace
            .getConfiguration("remote.SSH")
            .update("showLoginTerminal", true, vscode.ConfigurationTarget.Global)

        return true

    } else if (selectedAction === configSshAgentActionStr) {

        vscode.env.openExternal(
            vscode.Uri.parse(
                "https://aka.ms/vscode-remote/troubleshooting#_setting-up-the-ssh-agent"
            )
        )

        return false

    }
}

export default {
    NoSshAgentError,
    addKeyToSshAgent,
    handleNoSshAgentError,
}
