// @ts-ignore
export const isWindows = "win32" === process.platform

export default {
    isWindows
}
