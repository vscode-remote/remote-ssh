import vscode from "vscode"
import Dialog from "./dialog"
import Logger from "./logger"
import Tunnels, { Tunnel as TunnelType } from "./tunnels"
import { HostInfo } from "./host-info";

export const tunnelLabel = (tunnel: TunnelType) => {
    if (tunnel.name === "") {
        return `${tunnel.host.hostName}:${tunnel.remotePort}`
    } else {
        return tunnel.name
    }
}

export const tunnelDescription = (tunnel: TunnelType) => {

    const portStr = tunnel.localPort === tunnel.remotePort
        ? ""
        : `:${tunnel.localPort}`

    const labelStr = "" === tunnel.name
        ? ""
        : `${tunnel.host.hostName}:${tunnel.remotePort} `

    return `${labelStr}forwarded to localhost${portStr}`

}

class TunnelPicker implements vscode.QuickPickItem {

    tunnel: TunnelType;

    constructor(tunnel: TunnelType) {
        this.tunnel = tunnel;
    }

    get label() {
        return tunnelLabel(this.tunnel)
    }

    get description() {
        return tunnelDescription(this.tunnel)
    }

    get localPort() {
        return this.tunnel.localPort
    }

    closeTunnel() {
        this.tunnel.close()
    }

    get remoteAddress() {
        if (this.tunnel.name === "") {
            return `${this.tunnel.host.hostName}:${this.tunnel.remotePort}`
        } else {
            return `${this.tunnel.name} (${this.tunnel.host.hostName}:${this.tunnel.remotePort})`
        }
    }

}

class CreateNewForwardCommandPicker implements vscode.QuickPickItem {

    label: string;

    alwaysShow: boolean;

    constructor() {
        this.label = "Create New Forward..."
        this.alwaysShow = true
    }

}

export const openTunnel = async (extensionContext: vscode.ExtensionContext, hostinfo: HostInfo, canOpenInBrowser: boolean, logger: Logger) => {

    const remotePort = await Dialog.askForRemotePort(extensionContext, hostinfo, logger)

    if (remotePort === null) {
        return
    }

    const forwarderName = await Dialog.askForForwarderName(hostinfo, remotePort, logger)

    if (forwarderName !== null) {

        const options = {
            location: vscode.ProgressLocation.Notification,
            title: `Setting up port forwarding from ${hostinfo.hostName}`,
            cancellable: false
        }

        return vscode.window.withProgress(options, async (progress) => {

            let localPort: number;

            try {
                localPort = await Tunnels.createTunnel({
                    host: hostinfo,
                    remotePort: remotePort,
                    name: forwarderName,
                    internal: false,
                    progress: progress,
                    logger: logger
                })
            } catch (err) {
                vscode.window.showErrorMessage(err.message)
                return
            }

            const address = `localhost:${localPort}`;

            if (canOpenInBrowser) {

                const actionCopyAddress = "Copy Address"
                const actionOpenInBrowser = "Open in Browser"

                const message = `${hostinfo.hostName}:${remotePort} is now available as ${address}`

                vscode.window
                    .showInformationMessage(message, actionCopyAddress, actionOpenInBrowser)
                    .then((selectedAction) => {

                        if (selectedAction === actionCopyAddress) {

                            vscode.env.clipboard.writeText(address)

                        } else if (selectedAction === actionOpenInBrowser) {

                            vscode.env.openExternal(
                                vscode.Uri.parse(`http://${address}`)
                            )

                        }

                    })

            }

            return address;
        })

    } else {
        return
    }

}

interface CommandPickResult {
    type: "command";
    value: "create";
}

interface TunnelPickResult {
    type: "address",
    value: string;
}

type PickResult = TunnelPickResult | CommandPickResult;

const getPickTunnelResult = async (logger?: Logger): Promise<PickResult> => {

    return new Promise((resolve, reject) => {

        const activeTunnels = Tunnels.getActiveTunnels()

        if (activeTunnels.length === 0) {
            resolve({
                type: "command",
                value: "create"
            })
            return
        }

        let pickers: (CreateNewForwardCommandPicker | TunnelPicker)[] = activeTunnels.map(tunnel => new TunnelPicker(tunnel))

        const commandPicker = new CreateNewForwardCommandPicker()
        pickers.push(commandPicker)

        const quickpick = vscode.window.createQuickPick()
        quickpick.items = pickers
        quickpick.title = "Copy Local Forwarding Address or Create New Forward"

        let accepted = false

        quickpick.onDidAccept(async () => {
            accepted = true
            quickpick.busy = true

            const item = quickpick.selectedItems[0]

            let result: PickResult;
            if (item === commandPicker) {
                result = {
                    type: "command",
                    value: "create"
                }
            } else {
                result = {
                    type: "address",
                    value: `localhost:${(item as TunnelPicker).localPort}`
                }
            }

            quickpick.busy = false
            quickpick.hide()
            quickpick.dispose()

            resolve(result)

        })

        quickpick.onDidHide(() => {
            if (!accepted) {
                resolve(null)
            }
        })

        quickpick.show()
    })
}

export const getTunnelAddress = async (extensionContext: vscode.ExtensionContext, hostinfo: HostInfo, logger: Logger) => {

    const pickResult = await getPickTunnelResult()

    if (pickResult && pickResult.type === "address") {

        vscode.env.clipboard.writeText(`${pickResult.value}`)
        vscode.window.showInformationMessage(`"${pickResult.value}" copied to clipboard`)
        return pickResult.value

    } else if (pickResult && pickResult.type === "command" && pickResult.value === "create") {

        const address = await openTunnel(extensionContext, hostinfo, true, logger)
        return address

    } else {
        return
    }

}

export const closeTunnel = async (logger: Logger): Promise<number | null> /** localPort */ => {

    return new Promise((resolve, reject) => {

        let tunnelPickers: TunnelPicker[] = Tunnels.getActiveTunnels().map(tunnel => new TunnelPicker(tunnel))

        if (tunnelPickers.length === 0) {
            vscode.window.showInformationMessage("No ports are currently forwarded")
            resolve(null)
        }

        const quickpick: vscode.QuickPick<TunnelPicker> = vscode.window.createQuickPick()
        quickpick.items = tunnelPickers
        quickpick.title = "Select the port that should no longer be forwarded"

        let accepted = false

        quickpick.onDidAccept(async () => {
            accepted = true
            quickpick.busy = true

            const item = quickpick.selectedItems[0]
            item.closeTunnel()

            vscode.window.showInformationMessage(`Port ${item.remoteAddress} is no longer forwarded`)

            quickpick.busy = false
            quickpick.hide()
            quickpick.dispose()

            resolve(item.localPort)
        })

        quickpick.onDidHide(() => {
            if (!accepted) {
                resolve(null)
            }
        })

        quickpick.show()

    })

}

export default {
    tunnelLabel,
    tunnelDescription,
    getTunnelAddress,
    openTunnel,
    closeTunnel,
}
