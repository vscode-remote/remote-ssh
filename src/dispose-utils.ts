
import vscode from "vscode"

type EventEmitter = NodeJS.EventEmitter;
type Disposable = vscode.Disposable;

export const addDisposableListener = (event: EventEmitter, name: string | symbol, listener: (...args: any[]) => void) => {
    event.on(name, listener)
    return {
        dispose() {
            event.removeListener(name, listener)
        }
    }
}

export const dispose = (disposableList: Disposable[] | Disposable, ...moreDisposableList: Disposable[]) => {

    if (Array.isArray(disposableList)) {

        disposableList.forEach(disposable => {
            if (disposable) {
                disposable.dispose()
            }
        })
        return []

    } else if (moreDisposableList.length == 0) {

        const disposable = disposableList

        if (disposable) {
            disposable.dispose()
            return disposable
        } else {
            return
        }

    } else {
        dispose(disposableList)
        dispose(moreDisposableList)
        return []
    }
}

export default {
    addDisposableListener,
    dispose,
}
